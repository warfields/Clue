package gui;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import clueGame.*;

/**
 * 
 * @author Ross Starritt
 * @author Sam Warfield
 * 
 * This will be the display for the cards in the player's hand
 *
 */
public class HandGUI extends JPanel{
	
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a panel with the human player's cards
	 */
	public HandGUI() {
		//Grid Layout
		setLayout(new GridLayout(0,1));
		
		//get all players and find the human player
		ArrayList<Player> allPlayers = Board.getInstance().getPlayers();
		Player readyPlayerOne = null;
		
		//Go find that person
		for (Player aPerson : allPlayers) {
			if (aPerson.getClass().getName() == "clueGame.HumanPlayer") {
				readyPlayerOne = aPerson;
			}
		}
		
		ArrayList<Card> hand = readyPlayerOne.getHand();
		
		//For every card in the player's hand add it the handgui panel
		for (Card aCard : hand) {
			
			// Decide what the card type string is
			String typeName = new String();
			switch (aCard.getType()) {
			case WEAPON:
				typeName = "Weapon";
				break;
			case PERSON:
				typeName = "Person";
				break;
			case ROOM:
				typeName = "Room";
				break;
			case NONE:
				break;
			}
			
			//Create the JPanel that will be sent to the main gui 
			JPanel peoplePanel = new JPanel();
			JTextField peopleCard = new JTextField(aCard.getName());
			peopleCard.setEditable(false);
			peoplePanel.add(peopleCard);
			peoplePanel.setBorder(new TitledBorder(new EtchedBorder(), typeName));
			add(peoplePanel);
		}
		
		//sets the border that goes around the player cards
		setBorder(new TitledBorder(new EtchedBorder(), "Cards"));
	}
	
	// only used for testing
	public static void main(String[] args) {
		// Create a JFrame with all the normal functionality
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // end program on close
		frame.setTitle("hand");
		frame.setSize(200, 700);
		// Create the JPanel and add it to the JFrame
		HandGUI gui = new HandGUI();
		frame.add(gui, BorderLayout.CENTER);
		// Now let's view it
		frame.setVisible(true);
	}	
}

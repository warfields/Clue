package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import clueGame.*;

public class GuessGui extends JDialog {

	private static final long serialVersionUID = 1L;

	private Solution soln = new Solution();

	public GuessGui() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(4,2));

		//room
		JLabel room = new JLabel("Room");
		panel.add(room);
		JComboBox<String> whereAmI = new JComboBox<String>();
		ArrayList<String> where = Board.getInstance().getRooms();
		for (String w: where) {
			whereAmI.addItem(w);
		}
		panel.add(whereAmI);

		//person
		JLabel person = new JLabel("Person");
		panel.add(person);
		JComboBox<String> people = new JComboBox<String>();
		ArrayList<Player> players = Board.getInstance().getPlayers();
		for (Player p: players) {
			people.addItem(p.getName());
		}
		panel.add(people);

		//weapon
		JLabel weapon = new JLabel("Weapon");
		panel.add(weapon);
		JComboBox<String> weapons = new JComboBox<String>();
		ArrayList<String> stabies = Board.getInstance().getWeapons();
		for (String w: stabies) {
			weapons.addItem(w);
		}
		panel.add(weapons);
		soln = null;
		//submit and cancel buttons
		JButton submit = new JButton("Submit");
		class SubmissionListener implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				soln = new Solution();
				Card r = new Card(CardType.ROOM, String.valueOf(whereAmI.getSelectedItem()));
				Card w = new Card(CardType.WEAPON, String.valueOf(weapons.getSelectedItem()));
				Card p = new Card(CardType.PERSON, String.valueOf(people.getSelectedItem()));
				soln.setPerson(p);
				soln.setWeapon(w);
				soln.setRoom(r);
				setVisible(false);

				if(Board.getInstance().checkAccusation(soln)) {
					new SplashScreen("You Win!");
					System.exit(0);
				} else {
					new SplashScreen("You Lose :(");
					Player human = Board.getInstance().getHuman();
					Board.getInstance().getPlayers().remove(human);
					Board.getInstance().getHumanTargeted().clear();
					for (Card c: human.getHand()) {
						for (Player pl: Board.getInstance().getPlayers()) {
							pl.seeCard(c);
						}
					}
				}
			}
		}

		class CloseListener implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		}

		submit.addActionListener(new SubmissionListener());
		panel.add(submit);
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new CloseListener());
		panel.add(cancel);
		add(panel);
		setSize(300, 300);
		setVisible(true);
		setAlwaysOnTop(true);
	}

	public GuessGui(String Room) {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(4,2));

		//room
		JLabel room = new JLabel("Room");
		panel.add(room);
		JLabel whereAmI = new JLabel(Room);
		panel.add(whereAmI);

		//person
		JLabel person = new JLabel("Person");
		panel.add(person);
		JComboBox<String> people = new JComboBox<String>();
		ArrayList<Player> players = Board.getInstance().getPlayers();
		for (Player p: players) {
			people.addItem(p.getName());
		}
		panel.add(people);

		//weapon
		JLabel weapon = new JLabel("Weapon");
		panel.add(weapon);
		JComboBox<String> weapons = new JComboBox<String>();
		ArrayList<String> stabies = Board.getInstance().getWeapons();
		for (String w: stabies) {
			weapons.addItem(w);
		}
		panel.add(weapons);
		soln = null;
		//submit and cancel buttons
		JButton submit = new JButton("Submit");
		class SubmissionListener implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				soln = new Solution();
				Card r = new Card(CardType.ROOM, Room);
				Card w = new Card(CardType.WEAPON, String.valueOf(weapons.getSelectedItem()));
				Card p = new Card(CardType.PERSON, String.valueOf(people.getSelectedItem()));
				soln.setPerson(p);
				soln.setWeapon(w);
				soln.setRoom(r);
				Board.getInstance().getHuman().setSuggestion(soln);
				Board.getInstance().handleSuggestion(Board.getInstance().getHuman(), soln);
				Board.getInstance().repaint();
				setVisible(false);
			}
		}

		class CloseListener implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		}

		submit.addActionListener(new SubmissionListener());
		panel.add(submit);
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new CloseListener());
		panel.add(cancel);
		add(panel);
		setSize(300, 300);
		setVisible(true);
		setAlwaysOnTop(true);
	}

	public Solution getSoln() {
		return soln;
	}

	public static void main(String[] args) {
		GuessGui guess = new GuessGui();
		guess.setVisible(true);

	}

}

package gui;

import javax.swing.JOptionPane;

import clueGame.*;

public class SplashScreen extends JOptionPane {
	
	private static final long serialVersionUID = 1L;
	public SplashScreen() {
		HumanPlayer p1 =  Board.getInstance().getHuman();		
		//JOptionPane.INFORMATION_MESSAGE("Welcome to clue");
		showMessageDialog(null, "You are " + p1.getName() + ". Press OK to continue");
	}
	public SplashScreen(String message) {	
		//JOptionPane.INFORMATION_MESSAGE("Welcome to clue");
		showMessageDialog(null, message);
	}
}

package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.BeforeClass;
import org.junit.Test;

import clueGame.*;

public class GameSetupTests {

	// NOTE: I made Board static because I only want to set it up one 
	// time (using @BeforeClass), no need to do setup before each test.
	private static Board board;
	private static ArrayList<Card> theDeck;
	private static ArrayList<Player> allPlayers;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		// Board is singleton, get the only instance
		board = Board.getInstance();
		// set the file names to use my config files
		board.setConfigFiles("Clues.csv", "roomKey.txt", "people.csv", "weapons.csv");		
		// Initialize will load BOTH config files 
		board.initialize();
		
		theDeck = board.getDeck();
		allPlayers = board.getPlayers();
	}

	// This test is now defunct as the starting locations have changed
	/*
	@Test
	public void playerTests(){
		assertEquals(6, allPlayers.size()); // checks player count
		
		Boolean nameFlag = false; // to test that a name is included
		Boolean nameFlag2 = false; // to test that a name is included
		Boolean colorFlag = false; // to test that a color matches a specific player
		Boolean colorFlag2 = false; // to test that a color exists
		Boolean locationFlag = false;
		Boolean locationFlag2 = false;
		
		for (Player aPlayer : allPlayers) {
			if (aPlayer.getName().equals("Mr. Brown") ) {
				nameFlag = true;
				
				if (aPlayer.getColor() == Color.ORANGE) {
					colorFlag = true;
				}
				if ((aPlayer.getRow() == 0) && (aPlayer.getCol() == 0)) {
					locationFlag = true;
				}
				
			}
			if (aPlayer.getName().equals("Dr. Purple")) {
				nameFlag2 = true;
				
				if ((aPlayer.getRow() == 5) && (aPlayer.getCol() == 5)) {
					locationFlag2 = true;
				}
			}
			if (aPlayer.getColor() == Color.red) {
				colorFlag2 = true;
			}
		}
		
		assertTrue(nameFlag);
		assertTrue(nameFlag2);
		assertTrue(colorFlag);
		assertTrue(colorFlag2);
		assertTrue(locationFlag);
		assertTrue(locationFlag2);
	}
*/
	
    //Load/create the deck of cards
		//in board, changing load config files
	
	
		//      10       6         6
		//# of rooms + weapons + people
	@Test
	public void testNumCards() {
		assertEquals(22,theDeck.size());
		int numRooms = 0, numPerson = 0, numWeapons = 0;
		for (Card c: theDeck) {
			switch (c.getType()) {
			case ROOM:
				numRooms++;
				break;
			case PERSON:
				numPerson++;
				break;
			case WEAPON:
				numWeapons++;
				break;
			default:
				break;
			}
		}	
		assertEquals(numRooms,10);
		assertEquals(numPerson,6);
		assertEquals(numWeapons,6);
	}

		//Specific number of each
		//cards have the right type
	
		//weapon type, room type, person type
	
	@Test
	public void cardTypeChecker() { // Tests if the board properly loads card configs
		
	}
	
		//has name
	@Test
	public void cardNameCheck() {
		for (Card aCard : theDeck) {
			assertFalse(aCard.getName() == "");
		}
		
		Boolean nameFlag = false; // to test that a name is included
		Boolean roomFlag = false; // to test that a name is included
		Boolean weaponFlag = false; // to test that a name is included
		
		for (Card aCard : theDeck) {
			if (aCard.getName().equals("Mr. Brown") ) {
				nameFlag = true;
			}
			if (aCard.getName().equals("Treasure Room")) {
				roomFlag = true;
			}
			if (aCard.getName().equals("Potato Cannon")) {
				weaponFlag = true;
			}
			
		}
		assertTrue(nameFlag);
		assertTrue(roomFlag);
		assertTrue(weaponFlag);
	}
	
    @Test
    public void DealingTest() {
    	int totalCards = 0;
    	for(Player aPlayer : allPlayers) {
    		assertTrue(aPlayer.getHand().size() > 0);
    		totalCards += aPlayer.getHand().size();
    	}
    	
    	assertEquals(19, totalCards); // had to change because of a logic change. Deck should be size 19 by the time it gets here
    }


}

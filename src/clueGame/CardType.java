package clueGame;
/**
 * Enum for card type
 * @author Samuel Warfield
 * @author Ross Starritt
 */
public enum CardType {
	PERSON, WEAPON, ROOM, NONE
}

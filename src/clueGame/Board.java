package clueGame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import javax.swing.JPanel;

import gui.GuessGui;
import gui.SplashScreen;


/**
 * @author Ross Starritt
 * @author Samuel Warfield
 * This Class stores all board data for clue
 */

public class Board extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private int numRows;
	private int numColumns;
	public int JPanelWidth;
	public int JPanelHeight;
	public static int MAX_BOARD_SIZE = 5000;
	private BoardCell [][] board;
	private HashMap<Character , String> legend;
	private HashSet<BoardCell> targets;
	private HashMap<BoardCell, HashSet<BoardCell>> adjMatix;
	private String boardConfigFile;// = "Clues.csv";
	private String roomConfigFile;// = "roomKey.txt";
	private String peopleConfigFile;
	private String weaponConfigFile;
	private ArrayList<String> rooms;
	private ArrayList<Card> deck;
	private ArrayList<Player> players;
	private ArrayList<Player> cpuPlayers;
	private ArrayList<String> weapons;
	private Solution solution;
	private HumanPlayer human;
	private Die die;
	private Set<BoardCell> humanTargeted;
	private boolean getIsChosen;
	
	private class Die {
		static final int MAX_ROLL = 6;
		Random random;
		int roll;
		
		Die() {
			random = new Random();
		}
		
		public int nextRoll() {
			roll = random.nextInt(MAX_ROLL) + 1;
			return roll;
		}
	}

	/**
	 * Initialize will load BOTH config files 
	 */
	public void initialize() { 
		deck.clear(); // makes sure the proper cards are loaded
		cpuPlayers = new ArrayList<Player>();
		weapons = new ArrayList<String>();
		try {
			loadRoomConfig();
			loadBoardConfig();
			loadPeopleConfig();
			loadWeaponConfig();
		} catch (BadConfigFormatException e) {
			System.out.println(e.getMessage());
			System.exit(-1);
		} catch (FileNotFoundException e) {
			System.out.println("Nescary configuration files not found, check board csv and room config files");
			System.exit(-1);
		}

		dealCards(players);
		
		
		JPanelWidth = numColumns*BoardCell.BOX_WIDTH;
		JPanelHeight = numColumns*BoardCell.BOX_HEIGHT;
		setPreferredSize(new Dimension(JPanelWidth,JPanelHeight));
		
		die = new Die();
		addMouseListener(new PlayerTargeter());
		humanTargeted = new HashSet<BoardCell>();
		getIsChosen = false; //  flag is mouse event was successful
	}
	
	private class PlayerTargeter implements MouseListener{
		@Override
		public void mouseClicked(MouseEvent event) {
			Point location = event.getPoint();
			BoardCell celliboi = getCellAt(location.y / BoardCell.BOX_HEIGHT , location.x / BoardCell.BOX_WIDTH);
			if (humanTargeted.contains(celliboi)) {
				getIsChosen = true;
				human.setCol(celliboi.getCol());
				human.setRow(celliboi.getRow());
				
				if (celliboi.getInitial() != 'W') {
					new GuessGui(legend.get(celliboi.getInitial()));
				}
				
				humanTargeted.clear();
				repaint();
			} else {
				new SplashScreen("Please Select a Valid Tile Press OK to continue");
			}
		}
		@Override
		public void mouseEntered(MouseEvent arg0) {}

		@Override
		public void mouseExited(MouseEvent arg0) {}

		@Override
		public void mousePressed(MouseEvent arg0) {}

		@Override
		public void mouseReleased(MouseEvent arg0) {}
		
	}

	public boolean isGetIsChosen() {
		return getIsChosen;
	}

	public void setGetIsChosen(boolean getIsChosen) {
		this.getIsChosen = getIsChosen;
	}

	/**
	 * Loads the weapons
	 */
	public void loadWeaponConfig() throws BadConfigFormatException, FileNotFoundException {
		String curLine;
		String[] line;

		FileReader reader = new FileReader(weaponConfigFile);
		Scanner fin = new Scanner(reader);

		while(fin.hasNext()) { // String Parser
			curLine = fin.nextLine();
			line = curLine.split(",");
			deck.add(new Card(CardType.WEAPON,line[0]));
			weapons.add(line[0]);
		}
		fin.close();
	}

	/**
	 * Loads Clue Characters
	 */
	public void loadPeopleConfig() throws BadConfigFormatException, FileNotFoundException  {
		String curLine;
		String[] line;
		players.clear();

		FileReader reader = new FileReader(peopleConfigFile);
		Scanner fin = new Scanner(reader);

		while(fin.hasNext()) { // String Parser
			curLine = fin.nextLine();
			line = curLine.split(",");
			//System.out.println(curLine);

			//System.out.println(line.length);
			deck.add(new Card(CardType.PERSON,line[0]));

			if (line[4].equals("human")) {
				human = new HumanPlayer(line[0],line[1], Integer.parseInt(line[2]), Integer.parseInt(line[3]));
				players.add(human);
			} else { //Else CPU Player
				ComputerPlayer newCPU = new ComputerPlayer(line[0],line[1], Integer.parseInt(line[2]), Integer.parseInt(line[3]));
				players.add(newCPU);
				cpuPlayers.add(newCPU);
			}
		}
		fin.close();
	}

	/**
	 * This loads the room legend
	 */
	public void loadRoomConfig() throws BadConfigFormatException, FileNotFoundException {
		String curLine;
		String[] line;
		rooms = new ArrayList<String>();

		legend.clear();
		FileReader reader = new FileReader(roomConfigFile);
		Scanner fin = new Scanner(reader);

		while(fin.hasNext()) { // String Parser
			curLine = fin.nextLine();
			line = curLine.split(",");
			legend.put((line[0].charAt(0)), line[1].substring(1));
			if (!line[2].equals(" Card") && !line[2].equals(" Other")) {
				fin.close();
				BadConfigFormatException e = new BadConfigFormatException("Rooms cannot be of type" + line[2]);
				throw e;
			}

			if (line[2].equals(" Card")) {
				deck.add(new Card(CardType.ROOM, line[1].substring(1)));
				rooms.add(line[1].substring(1));
			}
		}
		fin.close();
	}

	/**
	 * This loads the csv with the game board layout
	 */
	public void loadBoardConfig() throws BadConfigFormatException, FileNotFoundException {
		String curLine;
		ArrayList<String[]> cellTypes = new ArrayList<String[]>();

		FileReader reader = new FileReader(boardConfigFile);
		Scanner fin = new Scanner(reader);
		int counter = 0; // keeps track of how many rows there are

		while(fin.hasNext()) { // Actual Parser
			curLine = fin.nextLine();
			cellTypes.add(curLine.split(","));
			if (cellTypes.get(counter).length != cellTypes.get(0).length) {
				fin.close();
				throw new BadConfigFormatException("Collumn missmatch in config");
			}
			counter++;
		}
		numColumns = cellTypes.get(0).length;
		numRows = cellTypes.size();

		board = new BoardCell[numRows][numColumns];

		for (int j = 0; j < numRows; j++) {// resurve 
			for (int i = 0; i < numColumns; i++) {
				board[j][i] = new BoardCell(i,j);// i refers to x, j refers to y
				// arrays are accessed as array[row][column] or array[y][x]
			}
		}

		for (int j = 0; j < numRows; j++) { // Sets the cell's door direction
			for (int i = 0; i < numColumns; i++) {
				if (!legend.containsKey(cellTypes.get(j)[i].charAt(0))) {
					fin.close();
					throw new BadConfigFormatException("Cell type not in Legend");
				}
				board[j][i].setInitial(cellTypes.get(j)[i].charAt(0));
				if (cellTypes.get(j)[i].length() > 1) {
					switch (cellTypes.get(j)[i].charAt(1)) {
					case 'U': board[j][i].setDoorDirection(DoorDirection.UP);
					break;

					case 'D': board[j][i].setDoorDirection(DoorDirection.DOWN);
					break;

					case 'R': board[j][i].setDoorDirection(DoorDirection.RIGHT);
					break;

					case 'L': board[j][i].setDoorDirection(DoorDirection.LEFT);
					break;
					
					case 'Q': board[j][i].setLable(true);
					break;	
					}
				}
			}
		}

		fin.close();
		calcAdjacencies();
	}

	/**
	 * Finds adjacent cells to every cell on the board at the beginning of the game
	 */
	public void calcAdjacencies() {
		adjMatix.clear();
		HashSet<BoardCell> adjCells = new HashSet<BoardCell>();

		// A butt load of switch statements for determining adjacencies
		for (int x = 0; x < numColumns; x++) {
			for (int y = 0; y < numRows; y++) {
				adjCells = new HashSet<BoardCell>();

				//Below
				if (y-1 >= 0) {
					adjCells.add(board[y-1][x]);
				}
				//Above
				if (y+1 < numRows) {
					adjCells.add(board[y+1][x]);
				}
				//Left
				if (x-1 >= 0) {
					adjCells.add(board[y][x-1]);
				}
				//Right
				if (x+1 < numColumns) {
					adjCells.add(board[y][x+1]);
				}
				if (getCellAt(y,x).isRoom()) {
					adjCells.clear();
				}

				if (getCellAt(y,x).isDoorway()) {
					adjCells.clear();
					switch (getCellAt(y,x).getDoorDirection()) {
					case UP:
						adjCells.add(getCellAt(y-1,x));
						break;
					case DOWN:
						adjCells.add(getCellAt(y+1,x));
						break;
					case LEFT:
						adjCells.add(getCellAt(y,x-1));
						break;
					case RIGHT:
						adjCells.add(getCellAt(y,x+1));
						break;
					case NONE: break;
					}
				}

				Iterator<BoardCell> it = adjCells.iterator();
				while(it.hasNext()) {
					BoardCell celliBoi = it.next();

					if (celliBoi.isDoorway()) {
						switch (celliBoi.getDoorDirection()) {
						case UP:
							if (celliBoi.getRow() <= y) {
								it.remove();
							}
							break;

						case DOWN:
							if (celliBoi.getRow() >= y) {
								it.remove();
							}
							break;

						case LEFT:
							if (celliBoi.getCol() <= x) {
								it.remove();
							}
							break;

						case RIGHT:
							if (celliBoi.getCol() >= x) {
								it.remove();
							}
							break;

						case NONE: break;
						} 

					} else if (!celliBoi.isWalkWay()) {
						it.remove();
					}
				}
				adjMatix.put(getCellAt(y,x), adjCells);
			}
		}
	}

	/**
	 * Finds all viable spaces the player can move to
	 * @param cell The cell to find targets from.
	 * @param pathLength How many spaces can you move from the cell.
	 */
	public void calcTargets(BoardCell cell, int pathLength) {
		HashSet<BoardCell> visited = new HashSet<BoardCell>();
		/*
		 * for each adjacent cell, call tracking software
		 */		
		visited.add(cell);
		targets = targetingSoftware(cell, pathLength, visited);
	}

	/**
	 * Finds all viable spaces the player can move to
	 * @param y y coord of the cell the player is in
	 * @param x x coord of the cell the player is in
	 * @param distance How the player is moving
	 */
	public void calcTargets(int y, int x, int distance) { //For compatibility with Mark's code
		calcTargets(getCellAt(y,x), distance);
	}

	// Recursive pathing algorythm
	private HashSet<BoardCell> targetingSoftware(BoardCell startCell, int pathLength, HashSet<BoardCell> visited) {
		// for each adjacent cell, call tracking software, unless pathLength is 0
		HashSet<BoardCell> validMoves = new HashSet<BoardCell>();
		if (pathLength == 0) { // Base Case
			validMoves.add(startCell);
			return validMoves;
		}
		if (startCell.isDoorway() && !visited.contains(startCell)) { // Doorway case
			validMoves.add(startCell);
			return validMoves;
		}

		if(visited.containsAll(getAdjList(startCell)) ) { // Null Case
			validMoves.clear();
			return validMoves;
		}

		for (BoardCell celliBoi: getAdjList(startCell)) { // Recursive case
			if (visited.contains(celliBoi)) {
				continue;
			} else {

				HashSet<BoardCell> newVisited = new HashSet<BoardCell>();
				newVisited.addAll(visited);
				newVisited.add(startCell);
				validMoves.addAll(targetingSoftware(celliBoi, pathLength - 1, newVisited)); // recursive call

			}

		}
		return validMoves; // final return up the function stack
	}

	/**
	 * Set the locations of the config files
	 * @param boardConfigFile The path to the csv containning the layout
	 * @param roomConfigFile The path to the .txt file that has the room info
	 */
	public void setConfigFiles(String boardConfigFile, String roomConfigFile) {
		this.boardConfigFile = "" + boardConfigFile;
		this.roomConfigFile = roomConfigFile;

	}

	/**
	 * Set the locations of the config files
	 * @param boardConfigFile The path to the csv containning the layout
	 * @param roomConfigFile The path to the .txt file that has the room info
	 * @param people File path to people config
	 * @param weapons File path to weapon config
	 */
	public void setConfigFiles(String boardConfigFile, String roomConfigFile, String people, String weapons) {
		this.boardConfigFile = "" + boardConfigFile;
		this.roomConfigFile = roomConfigFile;
		this.peopleConfigFile = people;
		this.weaponConfigFile = weapons;
	}

	/**
	 * deals the deck in a random fashion uniformly among players.
	 * @param players the list of players to deal to
	 */

	public void dealCards(ArrayList<Player> players) {
		// use % and RNJesus to deal cards randomly to each player
		Random random = new Random();
		int card = 0;
		ArrayList<Card> toDeal = new ArrayList<Card>();
		toDeal.addAll(deck);
		generateSolution(toDeal);
		while(!toDeal.isEmpty()) {
			//deal
			for(Player p:players) {
				//deal a card to p
				if (toDeal.isEmpty()) {
					break;
				}
				card = random.nextInt(toDeal.size());
				p.deal(toDeal.get(card));
				toDeal.remove(card);
			}
		}	
	}


	/**
	 * Generates the final solution at the beginning of the game
	 * @param toDeal The deck about to be dealt to players.
	 */
	private void generateSolution(ArrayList<Card> toDeal) {
		// get a weapon, person, and room, then add to Solution
		solution = new Solution();
		Boolean w = false, p = false, r = false; 
		Iterator <Card> it = toDeal.iterator();
		while(it.hasNext()) {
			Card c = it.next();
			if(!w && c.getType() == CardType.WEAPON) {
				w = true;
				solution.setWeapon(c);
				it.remove();
			}
			else if(!p && c.getType() == CardType.PERSON) {
				p = true;
				solution.setPerson(c);
				it.remove();
			}
			else if(!r && c.getType() == CardType.ROOM) {
				r = true;
				solution.setRoom(c);
				it.remove();
			}
		}
	}

	public int getNumRows() {
		return numRows;
	}

	public void setNumRows(int numRows) {
		this.numRows = numRows;
	}

	public int getNumColumns() {
		return numColumns;
	}

	public void setNumColumns(int numColumns) {
		this.numColumns = numColumns;
	}

	public ArrayList<String> getWeapons() {
		return weapons;
	}

	public HashMap<Character, String> getLegend() {
		return legend;
	}

	public BoardCell getCellAt(int row, int col) {
		return board[row][col];
	}

	public HashSet<BoardCell> getTargets() {
		return targets;
	}

	/**
	 * Gets the adjacency list for a specific BoardCell
	 * @param ofInterest The cell that you're interested
	 * @return returns a set of Boardcells adjacent to the cell of interest
	 */
	public HashSet<BoardCell> getAdjList(BoardCell ofInterest){
		return adjMatix.get(ofInterest);
	}

	public HashSet<BoardCell> getAdjList(int y, int x){
		return adjMatix.get(getCellAt(y,x));
	}

	// variable used for singleton pattern
	private static Board theInstance = new Board();

	/**
	 * Private constructor for the singleton Board class
	 */
	private Board() {	
		boardConfigFile = "Clues.csv";
		roomConfigFile = "roomKey.txt";
		peopleConfigFile= "people.csv";
		weaponConfigFile = "weapons.csv";

		legend = new HashMap<Character , String>();
		adjMatix = new HashMap<BoardCell, HashSet<BoardCell>>();
		deck = new ArrayList<Card>();
		players = new ArrayList<Player>();

		initialize(); //load board files
		calcAdjacencies();

	} 
	/**
	 * this method returns the only Board singleton
	 * @return
	 */
	public static Board getInstance() {
		if (theInstance == null) {
			theInstance = new Board();
		}
		return theInstance;
	}

	public ArrayList<Card> getDeck() {
		return deck;
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public Solution getSolution() {
		return solution;
	}

	/**
	 * Checks that an accusation matches the actual solution
	 * @param suggestedSoln What the player thinks is the solution to the murder
	 * @return (Boolean) Is there Accusation correct?
	 */
	public boolean checkAccusation(Solution suggestedSoln) {
		return solution.equals(suggestedSoln);
	}

	/**
	 * This function hands the suggestion game mechanic of clue.
	 * @param suggester The player making the suggestion.
	 * @param suggestedSoln A Solution object with what the player thinks happened
	 * @return Returns an ArrayList of cards that disproves parts or all of a suggested solution
	 */
	public ArrayList<Card> handleSuggestion(Player suggester, Solution suggestedSoln) {
		ArrayList<Player> disprovees = new ArrayList<Player>(); //list of people to disprove from
		ArrayList<Card> cardReturn = new ArrayList<Card>();    //list of disproven cards
		
		//move accused to the room
		String nameToMove = suggestedSoln.getPerson().getName();
		Player personToMove = null;
		
		for (Player aPerson: players) {
			if (aPerson.getName() == nameToMove) {
				personToMove = aPerson;
			}
		}
		
		if (personToMove == null) {
			return null;
		}
		
		// move the accused
		personToMove.setBoardCell(suggester.getBoardCell());
		
		// make a list of all players that arn't the suggester
		disprovees.addAll(players);
		disprovees.remove(suggester);

		for (Player aPlayer : disprovees) {
			if (aPlayer.disproveSuggestion(suggestedSoln) != null) {
				cardReturn.add(aPlayer.disproveSuggestion(suggestedSoln));
			}
		}

		if(cardReturn.size() == 0) {// if no one can disprove then return null
			return null;
		}
		return cardReturn;
	}

	public ArrayList<Player> getCpuPlayers() {
		return cpuPlayers;
	}
	
	public ArrayList<String> getRooms() {
		return rooms;
	}
	
	/**
	 * Draws the board
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		//Draw Cells
		for (int i = 0; i < numColumns; i++) {
			for (int j = 0; j < numRows; j++) {
				board[j][i].draw(g);
			}
		}
		
		//Draw Targeted Cells
		for (BoardCell aCell: humanTargeted) {
			aCell.draw(g, Color.red);
		}
		
		//Draw People
		for (Player aPerson : players) {
			aPerson.draw(g);
		}
		
		//Draw Label
		for (int i = 0; i < numColumns; i++) {
			for (int j = 0; j < numRows; j++) {
				board[j][i].drawLable(g);
			}
		}
	}

	public HumanPlayer getHuman() {
		return human;
	}
	
	private int rollDie() {
		return die.nextRoll();
	}
	
	public int getDieRoll() {
		int roll = rollDie();
		
		while (roll == 0){
			rollDie();
		}
		
		return roll;
	}

	public Set<BoardCell> getHumanTargeted() {
		return humanTargeted;
	}

	public void setHumanTargeted(Set<BoardCell> humanTargeted) {
		this.humanTargeted = humanTargeted;
	}

	public boolean GetIsChosen() {
		return getIsChosen;
	}
}

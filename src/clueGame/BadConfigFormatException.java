package clueGame;

/**
 * If the config files are not correct this exception is thrown
 * @author Samuel Warfield
 * @author Ross Starritt
 */
public class BadConfigFormatException extends Exception{

	private static final long serialVersionUID = 3127262705121520249L;
	private String message;
	
	public BadConfigFormatException() {
		message = "Error: The config file is incompatible with this game.";
		return;
	}
	
	public BadConfigFormatException(String specifier) {
		message = new String("Error: The config file is incompatible.\nMessage: " + specifier);
	}

	public String getMessage() {
		return message;
	}
	
}

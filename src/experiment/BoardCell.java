package experiment;

public class BoardCell {
	protected int row;
	protected int col;

	/**
	 * Constructor
	 * @param col x coordinate
	 * @param row y coordinate
	 */
	public BoardCell(int col, int row){
		this.col = col;
		this.row = row;
	}

	/**
	 * Default Constructor
	 */
	public BoardCell(){
		col = 0;
		row = 0;
	}

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "BoardCell [row=" + row + ", col=" + col + "]";
	}
}

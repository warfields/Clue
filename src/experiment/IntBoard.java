package experiment;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Ross Starritt
 * @author Samuel Warfield
 *
 */
public class IntBoard {

	private Map<BoardCell, Set<BoardCell>> adjList; 
	private BoardCell[][] theBoard;
	private HashSet<BoardCell> targets;
	private int cols;
	private int rows;

	/**
	 * Default constructor makes a 4 x 4 test board
	 */
	public IntBoard() {
		super();
		cols = 4;
		rows = 4;
		adjList = new HashMap<BoardCell, Set<BoardCell>>();
		theBoard = new BoardCell[rows][cols];

		for (int x = 0; x < cols; x++) {
			for (int y = 0; y < rows; y++) {
				theBoard[x][y] = new BoardCell(x,y);
			}
		}
		this.calcAdjencencies();
		targets = new HashSet<BoardCell>();
	}

	/**
	 * Constructor
	 * @param rows How many rows does the board have?
	 * @param cols How many cols does the board have
	 */
	public IntBoard(int rows, int cols) {
		super();
		this.cols = cols;
		this.rows = rows;
		theBoard = new BoardCell[cols][rows];

		for (int x = 0; x < cols; x++) {
			for (int y = 0; y < rows; y++) {
				theBoard[x][y] = new BoardCell(x,y);
			}
		}
		this.calcAdjencencies();
		targets = new HashSet<BoardCell>();
	}

	/**
	 * Calculates all adjacent cells for each cell on the board. (Called in the constructor)
	 */
	public void calcAdjencencies(){
		HashSet<BoardCell> adjCells = new HashSet<BoardCell>();
		//System.out.println(rows);
		for (int x = 0; x < cols; x++) {
			for (int y = 0; y < rows; y++) {
				adjCells = new HashSet<BoardCell>();
				//Calc Adj cells

				//Below
				if (y-1 >= 0) {
					adjCells.add(theBoard[x][y-1]);
				}
				//Above
				if (y+1 < rows) {
					adjCells.add(theBoard[x][y+1]);
				}
				//Left
				if (x-1 >= 0) {
					adjCells.add(theBoard[x-1][y]);
				}
				//Right
				if (x+1 < cols) {
					adjCells.add(theBoard[x+1][y]);
				}

				//Add to adjList
				//System.out.println(x);
				//System.out.print(y);

				adjList.put(theBoard[x][y], adjCells);
			}
		}
	}

	/**
	 * Gets a set of adjacent cells
	 * @param theCell The cell that in question
	 * @return The neighbors of theCell
	 */
	public Set<BoardCell> getAdjList(BoardCell theCell){
		return adjList.get(theCell);
	}
	/**
	 * Calculates a where someone can move to from a certain cell
	 * @param startCell The cell where pathing starts.
	 * @param pathLength How long of a path is required.
	 */
	public void calcTargets(BoardCell startCell, int pathLength){
		HashSet<BoardCell> visited = new HashSet<BoardCell>();
		/*
		 * for each adjacent cell, call tracking software
		 */		
		targets = targetingSoftware(startCell, pathLength, visited);

	}

	// Recursive pathing algorythm
	private HashSet<BoardCell> targetingSoftware(BoardCell startCell, int pathLength, HashSet<BoardCell> visited) {
		// for each adjacent cell, call tracking software, unless pathLength is 0
		HashSet<BoardCell> validMoves = new HashSet<BoardCell>();
		if (pathLength == 0) {
			validMoves.add(startCell);
			return validMoves;
		}
		//visited.add(startCell);
		for (BoardCell celliBoi: getAdjList(startCell)) {
			if (visited.contains(celliBoi)) {
				continue;
			} else {

				HashSet<BoardCell> newVisited = new HashSet<BoardCell>();
				newVisited.addAll(visited);
				validMoves.addAll(targetingSoftware(celliBoi, pathLength - 1, newVisited));

			}
			visited.add(celliBoi);
		}
		return validMoves; 
	}

	/**
	 * Gets the set of locations that the player can move to
	 * @return A set of board cells that can be moved to
	 */
	public HashSet<BoardCell> getTargets() {
		return targets;
	}

	/**
	 * Getter for individual cells.
	 * @param x
	 * @param y
	 * @return The board cell at (x,y)
	 */
	public BoardCell getCell(int x, int y) {
		return theBoard[x][y];
	}
}
